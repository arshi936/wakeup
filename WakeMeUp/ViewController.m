//
//  ViewController.m
//  WakeMeUp
//
//  Created by Click Labs 107 on 9/28/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"

@interface ViewController ()
{
    HomeViewController *homeViewObject;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor orangeColor]];
    [self disclaimerView];
}
-(void)disclaimerView{
    UIView *disclaimerViewObject = [[UIView alloc]initWithFrame:CGRectMake(0, 180, 400, 250)];
    [disclaimerViewObject setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:disclaimerViewObject];
    
    UILabel *disclaimerLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 150, 50)];
    [disclaimerLabel setText:@"Disclaimer"];
    [disclaimerLabel setFont:[UIFont systemFontOfSize:20]];
    [disclaimerLabel setTextColor:[UIColor whiteColor]];
    [disclaimerViewObject addSubview:disclaimerLabel];
    
    UILabel *disclaimerContent = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 300, 120)];
    [disclaimerContent setText:@"This app does not gurantee that it will work!\n We are not liable for any problems or\n accidents that occur when using this app!"];
    disclaimerContent.shadowOffset=CGSizeMake(0, 8);
    disclaimerContent.numberOfLines=0;
    [disclaimerContent setFont:[UIFont systemFontOfSize:15]];
    [disclaimerContent setTextColor:[UIColor whiteColor]];
    [disclaimerViewObject addSubview:disclaimerContent];
    
    UIView *disclaimerViewLine = [[UIView alloc]initWithFrame:CGRectMake(0, 45, 400, 1)];
    [disclaimerViewLine setBackgroundColor:[UIColor whiteColor]];
    [disclaimerViewObject addSubview:disclaimerViewLine];
    
    UIView *disclaimerViewLine2 = [[UIView alloc]initWithFrame:CGRectMake(0, 170, 400, 1)];
    [disclaimerViewLine2 setBackgroundColor:[UIColor whiteColor]];
    [disclaimerViewObject addSubview:disclaimerViewLine2];
    
    UIButton *disclaimerButton = [[UIButton alloc]initWithFrame:CGRectMake(200, 180, 100, 40)];
    [disclaimerButton setTitle:@"Continue" forState:UIControlStateNormal];
    [disclaimerButton setBackgroundColor:[UIColor orangeColor]];
    //[disclaimerButton setFont:[UIFont systemFontOfSize:17]];
    [disclaimerButton addTarget:self action:@selector(continueButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [disclaimerViewObject addSubview:disclaimerButton];
}
-(void)continueButtonAction{
    homeViewObject=[[HomeViewController alloc]init];
    [self.navigationController pushViewController:homeViewObject=[[HomeViewController alloc]init] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
