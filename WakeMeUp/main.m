//
//  main.m
//  WakeMeUp
//
//  Created by Click Labs 107 on 9/28/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
