//
//  HomeViewController.m
//  WakeMeUp
//
//  Created by Click Labs 107 on 9/28/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import "HomeViewController.h"
#import "ViewController.h"
#import "TimerView.h"

@interface HomeViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>
@property(nonatomic,strong)UIPickerView *tripTypePickerViewObject;
@property(strong,nonatomic)UIPickerView *tiredLevelPickerViewObject;
@property(strong,nonatomic)UIPickerView *alertTypePickerViewObject;
@property(strong,nonatomic)NSMutableArray *tripTypeArrayObject;
@property(strong,nonatomic)NSMutableArray *tiredLevelArrayObject;
@property(strong,nonatomic)NSMutableArray *alertTypeArrayObject;
@end

@implementation HomeViewController{
    UIButton *tripTypeButton;
    UIButton *tiredLevelButton;
    UIButton *alertTypeButton;
    UILabel *tripTypeObject;
    TimerView *timerViewObject;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self homeView];
    [self.view setBackgroundColor:[UIColor orangeColor]];
    _tripTypeArrayObject = [[NSMutableArray alloc]initWithObjects:@"Family",@"Friends",@"Alone",nil];
    _tiredLevelArrayObject = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
    _alertTypeArrayObject = [[NSMutableArray alloc]initWithObjects:@"Normal",@"Random", nil];
}
#pragma marks- HomeViewControllermethods

-(void)homeView{
    UIView *homeViewObject = [[UIView alloc]initWithFrame:CGRectMake(0, 150, 400, 130)];
    [homeViewObject setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:homeViewObject];
   
    // Labels
    
    tripTypeObject = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 400, 40)];
    [tripTypeObject setText:@" Trip Type"];
    [tripTypeObject setFont:[UIFont systemFontOfSize:20]];
    [tripTypeObject setTextColor:[UIColor blackColor]];
    [tripTypeObject setBackgroundColor:[UIColor whiteColor]];
    tripTypeObject.userInteractionEnabled=true;
    [homeViewObject addSubview:tripTypeObject];
    
    UILabel *tiredLevelObject = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, 400, 40)];
    [tiredLevelObject setText:@" Tired Level"];
    [tiredLevelObject setFont:[UIFont systemFontOfSize:20]];
    [tiredLevelObject setTextColor:[UIColor blackColor]];
    [tiredLevelObject setBackgroundColor:[UIColor whiteColor]];
    tiredLevelObject.userInteractionEnabled=true;
    [homeViewObject addSubview:tiredLevelObject];
    
    UILabel *alertTypeObject = [[UILabel alloc]initWithFrame:CGRectMake(0, 95, 400, 40)];
    [alertTypeObject setText:@" Alert Type"];
    [alertTypeObject setFont:[UIFont systemFontOfSize:20]];
    [alertTypeObject setTextColor:[UIColor blackColor]];
    [alertTypeObject setBackgroundColor:[UIColor whiteColor]];
    alertTypeObject.userInteractionEnabled=true;
    [homeViewObject addSubview:alertTypeObject];
    
    // Buttons
    
    tripTypeButton = [[UIButton alloc]initWithFrame:CGRectMake(140, 5, 170, 30)];
    //[tripTypeButton setTitle:@"" forState:UIControlStateNormal];
    [tripTypeButton setBackgroundColor:[UIColor blackColor]];
    [tripTypeButton addTarget:self action:@selector(tripTypeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [tripTypeObject addSubview:tripTypeButton];
    
    
    tiredLevelButton = [[UIButton alloc]initWithFrame:CGRectMake(140, 5, 170, 30)];
    //[tiredLevelButton setTitle:@"" forState:UIControlStateNormal];
    [tiredLevelButton setBackgroundColor:[UIColor blackColor]];
    [tiredLevelButton addTarget:self action:@selector(tiredLevelAction) forControlEvents:UIControlEventTouchUpInside];
    [tiredLevelObject addSubview:tiredLevelButton];
    
    
    alertTypeButton = [[UIButton alloc]initWithFrame:CGRectMake(140, 5, 170, 30)];
    //[alertTypeButton setTitle:@"" forState:UIControlStateNormal];
    [alertTypeButton setBackgroundColor:[UIColor blackColor]];
    [alertTypeButton addTarget:self action:@selector(alertTypeAction) forControlEvents:UIControlEventTouchUpInside];
    [alertTypeObject addSubview:alertTypeButton];
    
    // Activate Button
    
    UIButton *activateButton = [[UIButton alloc]initWithFrame:CGRectMake(100, 280, 100, 60)];
    [activateButton setTitle:@"Activate" forState:UIControlStateNormal];
    [activateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [activateButton setBackgroundColor:[UIColor whiteColor]];
    //[disclaimerButton setFont:[UIFont systemFontOfSize:17]];
    [activateButton addTarget:self action:@selector(activateButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:activateButton];

}
-(void)tripTypeButtonAction{
    _tripTypePickerViewObject=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 400, 0, 0)];
    _tripTypePickerViewObject.showsSelectionIndicator=YES;
    _tripTypePickerViewObject.dataSource=self;
    _tripTypePickerViewObject.delegate=self;
    _tripTypePickerViewObject.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_tripTypePickerViewObject];
    self.tiredLevelPickerViewObject.hidden=YES;
    self.alertTypePickerViewObject.hidden=YES;
    
}
-(void)tiredLevelAction{
    _tiredLevelPickerViewObject=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 400, 0, 0)];
    _tiredLevelPickerViewObject.showsSelectionIndicator=YES;
    _tiredLevelPickerViewObject.dataSource=self;
    _tiredLevelPickerViewObject.delegate=self;
    _tripTypePickerViewObject.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_tiredLevelPickerViewObject];
    self.tripTypePickerViewObject.hidden=YES;
    self.alertTypePickerViewObject.hidden=YES;

}
-(void)alertTypeAction{
    _alertTypePickerViewObject=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 400, 0, 0)];
    _alertTypePickerViewObject.showsSelectionIndicator=YES;
    _alertTypePickerViewObject.dataSource=self;
    _alertTypePickerViewObject.delegate=self;
    _tripTypePickerViewObject.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_alertTypePickerViewObject];
    self.tiredLevelPickerViewObject.hidden=YES;
    self.tripTypePickerViewObject.hidden=YES;
}
-(void)activateButtonAction{
    timerViewObject=[[TimerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)];
    [timerViewObject callTimerMethod];
    [self.view addSubview:timerViewObject];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _alertTypeArrayObject.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView==self.tripTypePickerViewObject){
        return self.tripTypeArrayObject[row];
    }else if (pickerView==self.tiredLevelPickerViewObject){
        return self.tiredLevelArrayObject[row];
    }
    else{
        return self.alertTypeArrayObject[row];
    }
    
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView==self.tripTypePickerViewObject){
        [self->tripTypeButton setTitle:[self.tripTypeArrayObject objectAtIndex:row] forState:UIControlStateNormal];
    }else if (pickerView==self.tiredLevelPickerViewObject){
        [self->tiredLevelButton setTitle:[self.tiredLevelArrayObject objectAtIndex:row] forState:UIControlStateNormal];
    }else{
        [self->alertTypeButton setTitle:[self.alertTypeArrayObject objectAtIndex:row] forState:UIControlStateNormal];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
