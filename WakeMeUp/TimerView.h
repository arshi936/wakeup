//
//  TimerView.h
//  WakeMeUp
//
//  Created by Click Labs 107 on 9/29/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerView : UIView
-(void)callTimerMethod;
@end
